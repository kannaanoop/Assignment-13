package edu.ilstu;

import java.io.*;
import java.util.Scanner;

/**
 * A class to maintain an array of students
 */
public class StudentList
{
	// add appropriate instance variables here: need an array of Students
	//   and a way to keep track of how many students are in the list
	//   assume there will never be more than 100 students
	//   call your array stuArray
	private Student [] stuArray;
	private int numOfStudents = 0;
	private int MAX_STUDENTS_ALLOWED = 100;



	// provide a default constructor that sets up an empty student array
    public StudentList(){
    	super();
    	this.stuArray = new Student[100];
		this.numOfStudents = 0;
	}


	// provide a private helper method that accepts a Student's name and
	//	returns the index of the student in the array or -1 if the student
	//	is not in the array. This method should be called by the
	//	displayStudent method

	private int getStudentPosition( String studentName){
    	if( this.stuArray != null) {
    		int numOfStuds = this.stuArray.length;
			for(int i = 0; i < numOfStuds; i++) {
				if( this.stuArray[i].getName().equalsIgnoreCase(studentName)) {
					return i;
				}
			}
		}
    	return -1;
	}

	/**Reads a list of students from a file
	 * @param fileName
	 *   Name of the file to read from
	 */
	public void readList(String fileName)
	{
		// fill in code to read a list of students from a file
		// into the array -- this should end with StudentList
		// consisting of exactly those students in the file--so
		// the first student in the file will be the first student
		// in the array, and all operations on the list will only
		// affect the set of students in the file until additional
		// students are added using the addStudent method below.
		// Make sure you don't overfill the array.
		// For each student, create the Student object using the default
		// constructor and then use the read method of Student to get
		// the data from the file
		try {
			int record = 0;
			File file = new File(fileName);
			FileInputStream inputStream = new FileInputStream(file);
			Scanner scanner = new Scanner(inputStream);
			while(scanner.hasNextLine() && this.numOfStudents < MAX_STUDENTS_ALLOWED){
				Student student = new Student();
				student.read(scanner);
				this.stuArray[record] = student;
				record++;
				this.numOfStudents++;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" Error Occured while reading from file");
		}
	}

	/** Writes a list of students to a file
	 * @param fileName
	 *   Name of the file to write to
	 */
	public void writeList(String fileName)
	{
		PrintWriter outfile = null;
		try
		{
			outfile = new PrintWriter(new FileWriter(fileName));
			// use a for loop to write all of the Student objects from
			// the array to output.txt by calling the Student's write 
			// method and passing it outfile
			for( int i = 0; i < this.numOfStudents; i++) {
				this.stuArray[i].write(outfile);
			}
			outfile.close();
		}
		catch (IOException e)
		{
			System.out.println("An error occurred: "+e);
			System.out.println("The list of students was not written");
		}
	}

	/** Add a student to the end of the list
	 * @param aStudent
	 *   The student to add
	 */
	public void addStudent(Student aStudent)
	{
		// write code to add a student to the end of the list of students only if there is room
		if(this.numOfStudents < MAX_STUDENTS_ALLOWED) {
			this.stuArray[ this.numOfStudents  ] = aStudent;
			this.numOfStudents++;
		}
	}

	/**
	 * @param studentName
	 */
	public void displayStudent(String studentName)
	{
		// fill in missing pieces of the following code and uncomment it
		// add appropriate code to handle an incorrect name

		int index = getStudentPosition(studentName);
		if(index < 0 ) {
			System.out.println(" No Student found with name : " + studentName);
			return;
		}
		System.out.println(this.stuArray[index]);
	}

	// Write a method to count the number of freshmen, sophomores, juniors and seniors in the array 
	//  and print the counts to the screen.  The Student class has a getClassLevel method that 
	//  returns a String with the value "Freshman", "Sophomore", "Junior" or "Senior". Use an array 
	//  to store your counts.
	// add an appropriate javadoc comment
	public void printClassCounts()
	{
		String [] classLevels = {"Freshman","Sophomore","Junior","Senior"};
		int [] classLevelCounts = new int [classLevels.length];
		classLevelCounts[0] = 0;//freshman
		classLevelCounts[1] = 0;//sophomore
		classLevelCounts[2] = 0;//junior
		classLevelCounts[3] = 0;

		for( int i=0;i<this.numOfStudents;i++) {
			if("Freshman".equalsIgnoreCase(this.stuArray[i].getClassLevel())){
				classLevelCounts[0] = classLevelCounts[0] + 1;
			} else if("Sophomore".equalsIgnoreCase(this.stuArray[i].getClassLevel())){
				classLevelCounts[1] = classLevelCounts[1] + 1;
			} else if("Junior".equalsIgnoreCase(this.stuArray[i].getClassLevel())) {
				classLevelCounts[2] = classLevelCounts[2] + 1;
			} else {
				classLevelCounts[3] = classLevelCounts[3] + 1;
			}
		}

		for( int j= 0;j <  classLevelCounts.length; j ++) {
			System.out.println( classLevels[j]  + " Class Level Count  : " + classLevelCounts[j]);
		}
	}
}
