package edu.ilstu;

public class StudentListTester {
    public static void main(String args[]) {

        StudentList studentList = new StudentList();

        // Reading all students from file
        studentList.readList("students.txt");

        //display a student from list of students from file
        System.out.println(" ******** Display Student Ann Hawkins ********");
        studentList.displayStudent("Ann Hawkins");

        System.out.println(" ******** Display Class Level Counts ******** ");
        studentList.printClassCounts();

        // Adding a student
        Student student = new Student("Anoop Kuttan", "Arts Technology", 3.35, 50);
        studentList.addStudent(student);

        //display a student from list of students from file
        System.out.println(" ******** Display Student Anoop Kuttan ********");
        studentList.displayStudent("Anoop Kuttan");

        System.out.println(" ******** Writing all Students details to a file studentout.txt ******** ");
        studentList.writeList("studentout.txt");

        System.out.println(" ******** Display Class Level Counts ******** ");
        studentList.printClassCounts();

        // Add 100 students, but program can only allow to add upto 100 in total. Uncomment below code to test it.
        // Below block can only add 96 records only as there are already 4 before and it makes it total 100
//        for( int i=1;i <=100;i++ ) {
//            studentList.addStudent(new Student("Anoop Kuttan " +i, "Arts Technology", 3.35, 50));
//            studentList.writeList("studentout.txt");
//        }

    }
}
